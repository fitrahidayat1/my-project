function convertRomanToNumber(num) {
    const romanMap = ["I","IV","V","IX","X","XL","L","C","CD","D","CM","M"];
    const numeralMap = [1,4,5,9,10,40,50,100,400,500,900,1000];

    for (let i=0; i<romanMap.length; i++) {
        
        for (let j=0; j< numeralMap.length; j++) {

            if (numeralMap[j] === num) {
                return romanMap[j];
            }
        }
    }

    return romanMap[num];
    
  }
  
  // driver code
  console.log(convertRomanToNumber(100)) // IV
  console.log(convertRomanToNumber(50)) // V