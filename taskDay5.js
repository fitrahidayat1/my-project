/**
 * Read file with Synchronous or Asynchronous, you can choose one method
 * Create new data to file dummyData.js
 * Don't replace existing data, only added the new data
 * new data ['mango','avocado','durian','guava']
 */

// write your code here
const fs = require('fs');

const readFile = fs.readFileSync('dummyData.js', 'utf8')
const data = readFile.replace(']','')
const newData = "'mango', 'avocado', 'durian', 'guava'";

const writeFile = () => {
    try {
        fs.writeFileSync('dummyData.js',data + `, ${newData}]`)
        console.log('Updated');
    } catch (err) {
        console.log('Error');
    }
}

writeFile();


