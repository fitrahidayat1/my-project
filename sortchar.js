function sortCharacters(words){
    
    // Kata di split untuk menjadi aray supaya bisa di sort, kemudian setelah di sort di join kembali.
    
    words = words.split('').sort().join('');
    return words
}

console.log(sortCharacters('baca'));
console.log(sortCharacters('hello'));
console.log(sortCharacters('truncate'));
console.log(sortCharacters('developer'));