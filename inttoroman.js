const intToRoman = (num) => {
    const romanMap = ["I","IV","V","IX","X","XL","L","C","CD","D","CM","M"];
    const numeralMap = [1,4,5,9,10,40,50,100,400,500,900,1000];

    const keys =romanMap.reverse();
    const values = numeralMap.reverse();
    const map = {}

    for (i=0; i<keys.length; i++) {
        map[keys[i]] = values[i];
    }
    //console.log(map);
 
    let result = '';
    
    for (key in map) {
      result = result + key.repeat(Math.floor(num / map[key]));
      num = num % map[key];

     // console.log(result);
     // console.log(num);
    }
    
    return result;
  };

 console.log(intToRoman(2939));
