// /**
//  * Mampu menggabungkan konsep Conditional dan Iteration dalam kasus sederhana
//  * Buat sebuah function yang bisa menerima parameter yang bisa melengkapi for loop.
//  * function yang dibuat hanya bisa menerima tipe data angkaber
//  * Buat pseudocode nya terlebih dahulu
//  */



// //Menentukan bilangan ganjil & genap
// let i = 10;
// for (let angka = 1; angka <= i; angka++) {
//   if (angka % 2 == 0) {
//     console.log(`${angka} : Angka Genap`);
//   } else {
//     console.log(`${angka} : Angka Ganjil`);
//   }
// }

function oddEvenNumber(num) {
  for (let j=1; j<=num; j++) {
    if(j%2) {
      console.log(`${j} is Ganjil`);
    } else {
      console.log(`${j} is Genap`);
    }
  }
}

oddEvenNumber(30);
//console.log(oddEvenNumber(20));


// console.log("--------------------------------------------");

// //    //Kelipatan 3 dengan pertambahan index 2
// for (let angka = 1; angka <= 10; angka += 2) {
//   if (angka % 3 === 0) {
//     //     // write your code here
//     console.log(`${angka} adalah kelipatan 3`);
//   } else {
//     console.log(angka);
//   }
// }
// // }

console.log("--------------------------------------------");

//  Menentukan faktor prima
let prime_factors = (angka, hasil = []) => {
  let root = Math.sqrt(angka);
  console.log(`angka : ${angka}`);
  console.log(`root : ${root}`);
  
  let x = 2;
  
  if (angka % x !== 0) {
    x = 3;
    
    while (angka % x !== 0 && (x = x + 2) < root) {}
  }

  x = x <= root ? x : angka;
  console.log(`x : ${x}`);
  console.log('===========================================');

  hasil.push(x);

  return x === angka ? hasil : prime_factors(angka/x, hasil);
  //return hasil;
  //return prime_factors(angka/x, hasil)
};

const angka = 50;
console.log(`Faktor prima dari ${angka} adalah ${prime_factors(angka, hasil = [])}`);
