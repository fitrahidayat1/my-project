function pigLatin(words) {
  const vocalWord = ["a", "i", "u", "e", "o"];
  words = words.toLowerCase();

  //Cek jumlah kata yang ada
  if (words.match(/ /)) {
      var countWords = words.split(' ');
      var result = '';
      for (let i = 0; i < countWords.length; i++) {

          //Jika bukan kata yang pertama tambah spasi di depan
          if (i > 0) {
              result += ' ';
          }
          result += pigLatinInput(countWords[i]);
      }
      return result
  }
  return pigLatinInput(words);

  function pigLatinInput(words){

      // Jika huruf pertama adalah vokal,
      if (vocalWord.includes(words[0])) {        
          return words;
      } else {
          for(let i=0; i<=words.length; i++){
              if (vocalWord.includes(words[i])){
                  arr = words.split('')
                  splice = arr.splice(0,i)
                  return arr.join('') + splice.join('') + 'ay'

              }
          }
      }
  }
}
console.log(pigLatin('apel'));
console.log(pigLatin('food'));
console.log(pigLatin('gloove'));
console.log(pigLatin('beli makanan'));
console.log(pigLatin('Snap'));
